#!/usr/bin/env node

import express from 'express';
import path from 'path';
import http from 'http';
var app = express();

app.use('/api', require('./api'));

app.use('/', express.static(__dirname + '/../client'));

app.route('/*').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/../client/index.html'));
});

var server = http.createServer(app);
var port = process.env.PORT || 9000;
server.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});