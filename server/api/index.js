import http from 'http';
import request from 'request';

var express = require('express');
var router = express.Router();

router.get('/lookup', handleLookup);

function handleLookup(req, res) {
    var url = `http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=${req.query.code}`;
    request.get(url, function (error, response, body) {
        if (response.statusCode != 200) {
            res.writeHead(response.statusCode);
        } else {
            res.writeHead(200, {"Content-Type": "application/json"});
        }
        res.end(body);
    });
}

router.get('/chart', handleChart);
function handleChart(req, res) {
    var parameters = {
        "Normalized": false,
        "NumberOfDays": req.query.days,
        "DataPeriod": "Day",
        "Elements": [{
            "Symbol": req.query.code,
            "Type": "price",
            "Params": ["c"]
        }]
    };
    var url = encodeURI('http://dev.markitondemand.com/MODApis/Api/v2/InteractiveChart/json?parameters=' + JSON.stringify(parameters));
    request.get(url, function (error, response, body) {
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(body);
    });

}

module.exports = router;