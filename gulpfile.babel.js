'use strict';

import gulp from 'gulp';
import {stream as wiredep} from 'wiredep';
import del from 'del';

import gulpLoadPlugins from 'gulp-load-plugins';
var $ = gulpLoadPlugins();

var browserSync = require('browser-sync').create();

var path = {};
path.build = 'build';
path.client = 'client';
path.server = 'server';
path.buildClient = `${path.build}/${path.client}`;
path.buildServer = `${path.build}/${path.server}`;
path.serverjs = `${path.buildServer}/server.js`;


gulp.task('default', ['browser-sync']);

var timeout;
gulp.task('nodemon', ['bower', 'babel-server', 'scripts', 'styles', 'html', 'watch'], function (cb) {
    var started = false;
    return $.nodemon({
        script: path.serverjs,
        env: {'NODE_ENV': process.env.NODE_ENV || 'development'},
        watch: path.buildServer
    }).on('start', function () {
        if (!started) {
            cb();
            started = true;
        }
    }).on('restart', function () {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            // Timeout to wait for server to listen on port 9000
            browserSync.reload({stream: false});
        }, 500);
    }).on('log', function (log) {
        console.log('[nodemon]' + log.message);
    });
});

gulp.task('clean', function () {
    del([`${path.build}/!(.git*|.openshift|Procfile)**`])
});

gulp.task('browser-sync', ['nodemon'], function () {
    return browserSync.init(null, {
        proxy: 'http://localhost:9000',
        files: 'build/**/*.*'
    });
});

gulp.task('scripts', function () {
    return gulp.src('client/app/**/*.js')
        .pipe($.sourcemaps.init())
        .pipe($.ngAnnotate())
        .pipe($.babel())
        .pipe($.concat('app.js'))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('babel-server', function () {
    return gulp.src('server/**/*.js')
        .pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(path.buildServer));
});

// inject bower components
gulp.task('wiredep', function () {
    return gulp.src(`./${path.client}/index.html`)
        .pipe(wiredep({
            directory: './client/bower_components',
            ignorePath: '..'
        }))
        .pipe(gulp.dest(`./${path.client}`));
});

gulp.task('bower', ['wiredep'], function () {
    return gulp.src(`${path.client}/bower_components/**/*.*`).pipe(gulp.dest(`${path.buildClient}/bower_components`));
});

gulp.task('styles', () => {
    return gulp.src(`${path.client}/**/*.scss`)
        .pipe($.sourcemaps.init())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.autoprefixer(), {browsers: ['last 1 version']})
        .pipe($.sourcemaps.write(), '.')
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('html', function () {
    gulp.src(`${path.client}/**/*.html`)
        // .pipe($.jade())
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('watch', function () {
    gulp.watch('server/**/*.js', ['babel-server']);
    gulp.watch(`./${path.client}/app/**/*.js`, ['scripts']);
    gulp.watch(`./${path.client}/**/*.scss`, ['styles']);
    gulp.watch(`./${path.client}/**/*.html`, ['html']);
    gulp.watch('bower.json', ['bower']);
});

gulp.task('copy:server', () => {
    return gulp.src([
            'package.json',
            'bower.json',
            '.bowerrc'
        ], {cwdbase: true})
        .pipe(gulp.dest(path.build));
});