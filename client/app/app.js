'use strict';

var app = angular.module('angularApp', ['ngRoute', 'firebase', 'ngMaterial']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController'
    });
    $routeProvider.otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
});

