app.directive('stock', function () {
    return {
        restrict: 'E',
        scope: {
            stock: '=',
            stocks: '='
        },
        templateUrl: 'app/main/stock.html'
    }
});