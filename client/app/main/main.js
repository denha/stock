
app.controller('MainController', function ($scope, $firebaseArray, $q, $mdToast, Stocks) {
    var ref = new Firebase("https://scorching-torch-2740.firebaseio.com/stock");
    $scope.stocks = $firebaseArray(ref);
    $scope.loaded = false;
    var seriesOptions = [];
    $scope.stocks.$loaded(function () {
        $scope.loaded = true;
        $scope.clicked();

        $scope.stocks.$watch(function (event) {
            console.log(event);
            switch (event.event) {
                case "child_added":
                    var i = $scope.stocks.length-1;
                    var stock = $scope.stocks[i];
                    Stocks.history(stock).then(function success(response) {
                        addData(response.data.query.results.quote, i);
                        draw();
                    });
                    break;
                case "child_removed":
                    var index = seriesOptions.findIndex(function(element) {
                        return element.id == event.key;
                    });
                    if (index !== -1) {
                        seriesOptions.splice(index, 1);
                        draw();
                    }
                    break;
            }
            // $scope.clicked();
        });
    });

    $scope.addStock = function (stock) {
        $scope.searchText = '';
        $scope.selectedItem= null;
        if (stock) {
            console.log('add stock ' + stock);
        } else {
            console.log('empty');
            return;
        }
        var exists = !$scope.stocks.every((value) => {
            return value.code != stock.Symbol;
        });
        if (!exists) {
            $scope.stocks.$add({code: stock.Symbol, name: stock.Name});
        } else {
            console.log('already exists!');
        }
    };

    $scope.clicked = ()=> {
        var requests = [];
        for (var i = 0; i < $scope.stocks.length; i++) {
            var stock = $scope.stocks[i];
            requests[i] = Stocks.history(stock);
        }
        $q.all(requests).then(function success(arr) {
            console.log('q success');
            // console.log(arr);
            for (var i = 0; i < arr.length; i++) {
                var data = arr[i].data.query.results.quote;
                addData(data, i);
            }
            draw();
        }, function error(response) {
            console.log('q error');
            console.log(response);
        });
    };

    function draw() {
        new Highcharts.StockChart({
            chart: {
                renderTo: 'container'
            },
            rangeSelector: {
                selected: 1
            },
            series: seriesOptions
        });
    }

    function addData(days, i) {
        // console.log(days);
        seriesOptions.push({
            id: $scope.stocks[i].$id,
            name: days[0].Symbol,
            data: days.map(function (_, i) {
                return [new Date(days[i].Date).getTime(), Number(days[i].Close)];
            }).reverse()
        });
    }

    $scope.find = (code)=> {
        return Stocks.find(code.toUpperCase())
            .then(function success(response) {
                console.log(response.data);
                var quote = response.data.query.results.quote;
                if (quote.Name == null) {
                    return {};
                }
                return [quote];
            }, function error(response) {
                console.log(response);
                showSimpleToast(response);
                return response;
            });
    };

    function showSimpleToast(message) {
        $mdToast.show($mdToast.simple()
            .textContent(message)
            // .position("top right")
            .hideDelay(3000)
        );
    }

});


app.factory('Stocks', function ($http) {
    return {
        history: function (stock) {
            var code = stock.code;

            function format(m) {
                return m.getUTCFullYear() + "-" +
                    ("0" + (m.getUTCMonth() + 1)).slice(-2) + "-" +
                    ("0" + m.getUTCDate()).slice(-2);
            }

            var today = format(new Date());
            var past = format(new Date(new Date().setDate(new Date().getDate() - 300)));
            var api = encodeURI(`https://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.historicaldata where symbol = "${code}" and startDate = "${past}" and endDate = "${today}"&format=json&env=store://datatables.org/alltableswithkeys&callback=JSON_CALLBACK`);
            return $http.jsonp(api);
        },
        find: function (code) {
            var api = encodeURI(`https://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quote where symbol in ("${code}")&format=json&env=store://datatables.org/alltableswithkeys&callback=JSON_CALLBACK`);
            return $http.jsonp(api);
        }
    }
});

// app.factory('Stocks', function ($http) {
//     return {
//         history: function (stock) {
//             var code = stock.code;
//             var parameters = {
//                 "Normalized": false,
//                 "NumberOfDays": 100,
//                 "DataPeriod": "Day",
//                 "Elements": [{
//                     "Symbol": code,
//                     "Type": "price",
//                     "Params": ["c"]
//                 }]
//             };
//             var api = encodeURI('http://dev.markitondemand.com/MODApis/Api/v2/InteractiveChart/jsonp?parameters=' + JSON.stringify(parameters) + '&callback=JSON_CALLBACK');
//             console.log(api);
//             return $http.jsonp(api);
//         },
//         find: function (code) {
//             // return $http.jsonp(api);
//             // var api = `http://dev.markitondemand.com/Api/v2/Lookup/json?input=GOOG`;
//             // return $http.get(`/api/lookup`, {params: {code: code}});
//             var api = `http://dev.markitondemand.com/Api/v2/Lookup/jsonp?input=${code}&callback=JSON_CALLBACK`;
//             return $http.jsonp(api);
//         }
//     }
// });